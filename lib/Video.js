import { store } from '../redux/store';
import {
  DOWNLOAD_ADD,
  DOWNLOAD_UPDATE_PROGRESS,
  DOWNLOAD_FINISHED,
  DOWNLOAD_REMOVE,
} from '../reducers/download';
import RNFetchBlob from 'rn-fetch-blob';
import type { VideoDetails } from './graphql/__generated__/VideoDetails';


export const deleteLocalItem = (video, download) => {
  const toDelete = download.videosMap[video.id];
  console.log(toDelete);
  if (toDelete){
    RNFetchBlob.fs.unlink(video.smallImage)
    .then(() => console.log("deleted image"))
    .catch((err) => console.log(err));
    RNFetchBlob.fs.unlink(toDelete.localLocation)
      .then(() => console.log("deleted local"))
      .catch((err) => console.log(err));    
  }
  store.dispatch({ type: DOWNLOAD_REMOVE, payload: video });
};

const DownloadImageToo = (video) => {
  RNFetchBlob
  .config({
    fileCache : true,
  })
  .fetch('GET', video.largeImage, {})
  .progress((received, total) => {
    console.log('progress', received / total);
  })
  .then((res) => {
    // the temp file path    
    const uri = Platform.OS === 'android' ? 'file://' + res.path() : '' + res.path();
    const newVideo = Object.assign({}, video, {
      largeImage: uri,
      smallImage: uri,
    });
    store.dispatch({ type: DOWNLOAD_FINISHED, payload: newVideo });
  });
}

// const CleanVideoItem = ({__typename, ...rest}: VideoDetails) =>
//   Object.assign({_id: rest.id, typename: __typename}, rest);

// Take a video object and put some salt and pepper around the
// details of what should be stored ( specifically the type above)
export const downloadVideo = (video: VideoDetails) => {
    
    let task = RNFetchBlob.config({ fileCache : true, appendExt : 'mp4' }).fetch('GET', video.hd, {});
    store.dispatch({ type: DOWNLOAD_ADD, payload: Object.assign({}, video, {task}) });
    task.progress((received, total) => {
      console.log('progress', received / total);
      store.dispatch({ type: DOWNLOAD_UPDATE_PROGRESS, video, payload: received / total})
    })
    .then((res) => {
      // the temp file path
      const uri = Platform.OS === 'android' ? 'file://' + res.path() : '' + res.path();
      console.log('The file saved to ', uri);
      const newVideo = Object.assign({}, video, {
        hd: uri,
        hls: uri,
        sd: uri,
        localLocation: uri
      });
      store.dispatch({ type: DOWNLOAD_FINISHED, payload: newVideo});
      DownloadImageToo(newVideo);
    })
    .catch((err) => console.log(err));  
};
