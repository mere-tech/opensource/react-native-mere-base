/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FolderDetails
// ====================================================

export type FolderDetails = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================