/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchDetails
// ====================================================

export type SearchDetails_folder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};

export type SearchDetails_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type SearchDetails = {
  __typename: "SearchResult",
  folder: ?SearchDetails_folder,
  video: ?SearchDetails_video,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================