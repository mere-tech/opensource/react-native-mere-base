/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GridFolderDetails
// ====================================================

export type GridFolderDetails_assets_subFolder_assets_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type GridFolderDetails_assets_subFolder_assets_subFolder_assets_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type GridFolderDetails_assets_subFolder_assets_subFolder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};

export type GridFolderDetails_assets_subFolder_assets_subFolder_assets = {
  __typename: "Asset",
  video: ?GridFolderDetails_assets_subFolder_assets_subFolder_assets_video,
  subFolder: ?GridFolderDetails_assets_subFolder_assets_subFolder_assets_subFolder,
  order: number,
  id: string,
};

export type GridFolderDetails_assets_subFolder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<GridFolderDetails_assets_subFolder_assets_subFolder_assets>,
};

export type GridFolderDetails_assets_subFolder_assets = {
  __typename: "Asset",
  video: ?GridFolderDetails_assets_subFolder_assets_video,
  subFolder: ?GridFolderDetails_assets_subFolder_assets_subFolder,
  order: number,
  id: string,
};

export type GridFolderDetails_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<GridFolderDetails_assets_subFolder_assets>,
};

export type GridFolderDetails_assets = {
  __typename: "Asset",
  order: number,
  id: string,
  subFolder: ?GridFolderDetails_assets_subFolder,
};

export type GridFolderDetails = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<GridFolderDetails_assets>,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================