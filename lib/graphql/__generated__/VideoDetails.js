/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: VideoDetails
// ====================================================

export type VideoDetails = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================