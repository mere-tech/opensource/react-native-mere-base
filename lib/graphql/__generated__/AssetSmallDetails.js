/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AssetSmallDetails
// ====================================================

export type AssetSmallDetails_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type AssetSmallDetails_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};

export type AssetSmallDetails = {
  __typename: "Asset",
  video: ?AssetSmallDetails_video,
  subFolder: ?AssetSmallDetails_subFolder,
  order: number,
  id: string,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================