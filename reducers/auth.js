/* @flow */
import AsyncStorage from '@react-native-community/async-storage';
import { store } from '../redux/store';

export type AuthState = {
  isLoaded: boolean,
  isAuthenticated: boolean,
  jwt: string,
};
export const AUTH_LOGIN = 'AuthState/AUTH_LOGIN';
export const AUTH_LOGOUT = 'AuthState/AUTH_LOGOUT';
export const AUTH_FIRST_BOOT = 'AuthState/AUTH_FIRST_BOOT'
AsyncStorage.getItem('id_token', (err, value) => {
  if (value !== null){
    console.log("trying to set login");
    store.dispatch({ type: AUTH_LOGIN, payload: value});
  }
  store.dispatch({ type: AUTH_FIRST_BOOT });
});


type actionList = {
  type: AUTH_LOGIN | AUTH_LOGOUT,
  jwt: string,
};

export default function AuthStateReducer(
  state: AuthState = { isAuthenticated: false, jwt: '', isLoaded: false },
  action: actionList,
) {
  switch (action.type) {
    case AUTH_FIRST_BOOT:
      return {
        ...state,
        isLoaded: true,
      }
    case AUTH_LOGIN:
      AsyncStorage.setItem('id_token', action.payload);
      return {
        ...state,
        isAuthenticated: true,
        jwt: action.payload,
      };
    case AUTH_LOGOUT:
      AsyncStorage.removeItem('id_token');
      return {
        ...state,
        isAuthenticated: false,
        jwt: '',
      };
    default:
      return state;
  }
};
