/* @flow */
import type { VideoDetails } from '../lib/graphql/__generated__/VideoDetails';

export type CompositeVideoItem = {
  lastWatchedDate: ?Date,
  firstWatchedSinceAuthorization: ?Date,
  authorizedDate: Date,
  partiallyWatched: boolean,
  timeLeftToWatchString: ?Number,
  localLocation: String,
  percentDownloaded: Number,
} | VideoDetais;

export type DownloadState = {
  videosMap: {}
};

export const DOWNLOAD_ADD = 'Download/add';
export const DOWNLOAD_UPDATE_PROGRESS = 'Download/progress';
export const DOWNLOAD_RENEW = 'Download/renew';
export const DOWNLOAD_REMOVE = 'Download/remove';
export const DOWNLOAD_FINISHED = 'Download/finished';


const newWithRemoved = (state, prop) => 
  Object.keys(state.videosMap).reduce((object, key) => {
    if (key !== prop) {
      object[key] = state.videosMap[key]
    }
    return object;
  }, {});
  

type actionList = {
  type: DOWNLOAD_ADD | DOWNLOAD_UPDATE_PROGRESS | DOWNLOAD_RENEW | DOWNLOAD_REMOVE,
  video: CompositeVideoItem
};

export default function DownloadStateReducer(
  state: DownloadState = { videosMap: {}},
  action: actionList,
  video?: VideoDetails
) {
  switch (action.type) {
    case DOWNLOAD_ADD:
      return {
        ...state,
        videosMap: Object.assign(state.videosMap, {[action.payload.id]: action.payload}),
      }
    case DOWNLOAD_UPDATE_PROGRESS:
      const foundVideo = state.videosMap[action.video.id];
      if (foundVideo){
        const toUpdate = Object.assign({}, foundVideo, {percentDownloaded: action.payload});
        return {
          ...state,
          videosMap: Object.assign(state.videosMap, {[action.video.id]: toUpdate})
        }
      }
      return state;
    case DOWNLOAD_FINISHED:
      const foundVideoFinished = state.videosMap[action.payload.id];
      if (foundVideoFinished){
        const toUpdateFinished = Object.assign({percentDownloaded: 1}, action.payload);
        return {
          ...state,
          videosMap: Object.assign(state.videosMap, {[action.payload.id]: toUpdateFinished})
        }
      }
      return state;
    case DOWNLOAD_REMOVE:
      const toRemove = state.videosMap[action.payload.id];
      console.log(toRemove);
      if (toRemove && toRemove.task && toRemove.task.cancel) {
        toRemove.task.cancel(err => console.log(err));
      }
      return {
        ...state,
        videosMap: newWithRemoved(state, action.payload.id)
      }
    default:
      return state;
  }
};
