/* eslint-disable import/no-unresolved */
import React from 'react';
import { Image, View, StyleSheet } from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';

import { colors, fonts } from '../../styles';

import HomeScreen from '../home/HomeViewContainer';
import AboutScreen from '../about/AboutViewContainer';
import WebViewScreen from '../webview/WebView';

const iconHome = require('mere-base-app/assets/images/tabbar/home-2.png');
const iconProfile = require('mere-base-app/assets/images/tabbar/login-4.png');
const iconPages = require('mere-base-app/assets/images/tabbar/login-4.png');
const iconComponents = require('mere-base-app/assets/images/tabbar/login-4.png');
const iconBook = require('mere-base-app/assets/images/tabbar/calendar.png');

const styles = StyleSheet.create({

});

const screens = 
{
  Home: {
    screen: HomeScreen,
  },
  About: {
    screen: AboutScreen,
  },
  Web: {
    screen: WebViewScreen,
  },
}

export default createDrawerNavigator(
  screens,
  {
    defaultNavigationOptions: ({ navigation }) => ({
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconSource;
        switch (routeName) {
          case 'Home':
            iconSource = iconHome;
            break;
          case 'Calendar':
            iconSource = iconProfile;
            break;
          case 'Web':
            iconSource = iconBook;
            break;
          case 'Pages':
            iconSource = iconPages;
            break;
          case 'Components':
            iconSource = iconComponents;
            break;
          default:
            iconSource = iconComponents;
        }
        return (
          <View 
            style={styles.tabBarItemContainer}
          >
            <Image
              resizeMode="contain"
              source={iconSource}
              style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
            />
          </View>
        );
      },
    }),
    drawerLockMode: 'locked-open',
    drawerType: "front",
    drawerPosition: "left",
    useNativeAnimations: true,
    drawerWidth: 200,
    animationEnabled: true,
  },
);
