import { compose, withState } from 'recompose';
import DownloadView from './DownloadView';

export default compose(withState(false))(
  DownloadView,
);
