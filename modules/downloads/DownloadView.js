/**
 * @flow
 */

import React from "react";
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import DownloadItem from "../../components/ListVideoItem";
import AppBackground from "../../components/AppBackground";
import { GetAllVideos } from "../../lib/Video";
import type { CompositeVideoItem } from "../../lib/Video";
import { pSBC } from "../../lib/Color";


const playbackTimeInSeconds = 172800;
const timeToAllowForDownloadInSeconds = 1209600;

const SecondsToString = (seconds) => {
  const hours   = Math.floor(seconds/ 3600);
  const minutes = Math.floor((seconds- (hours * 3600)) / 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  return hours + ':' + minutes;
}


// Map function to allow the video objects to have the time left
// mapped for display
const MapTimeInVideos = (video: CompositeVideoItem) => {
  if (video.firstWatchedSinceAuthorization && video.partiallyWatched) {
    const diff = new Date().getTime() - video.firstWatchedSinceAuthorization.getTime();
    const diffInSeconds = dif / 1000;
    const timeLeftToWatchString = SecondsToString(diffInSeconds);
    expired = diffInSeconds > playbackTimeInSeconds;
    return Object.assign(video, {expired, timeLeftToWatchString });
  } else if (video.firstWatchedSinceAuthorization) {
    const diff = new Date().getTime() - video.firstWatchedSinceAuthorization.getTime();
    const diffInSeconds = dif / 1000;
    const timeLeftToWatchString = SecondsToString(diffInSeconds);
    expired = diffInSeconds > timeToAllowForDownloadInSeconds;
    return Object.assign(video, {expired, timeLeftToWatchString });
  }
  return Object.assign(video, {expired: false, timeLeftToWatchString: "N/A"});
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  scrollContainer: {
    flex: 1,
  }
});


class Download extends React.Component<any, State> {
  render() {
    const { app, download } = this.props;
    const { environment } = app;
    const { thirdColor, mainColor } = environment;
    const darkerColor = pSBC(-0.8, thirdColor);
    return (
      <View style={Object.assign({}, styles.container, app.environment)}>
        <AppBackground />
        <ScrollView style={styles.scrollContainer} contentContainerStyle={{flexGrow: 1}}>
          <View style={{ height: 300 }} />
          <Text style={{ marginStart: 30, fontSize: 31, color: mainColor, fontWeight: 'bold'}} >
            Downloaded Items
          </Text>
          <View style={{ marginTop: 20 }}>
            <LinearGradient
              colors={[thirdColor, darkerColor]}
              style={{ height: "100%" }}
            >
              { download.videosMap && Object.keys(download.videosMap).length > 0 && (
                <>
                  {/* <Text style={{ marginStart: 30, fontSize: 24, fontWeight: 'bold', color: mainColor }}>Available</Text> */}
                  {
                    Object.values(download.videosMap).map(item => <DownloadItem showDelete showPoster video={item} key={item.id} />)
                  }
                </>
              )}
              {
                ( !download.videosMap || (download.videosMap && Object.keys(download.videosMap).length === 0)) && (
                  <Text style={{ margin: 30, fontSize: 15, color: mainColor, fontWeight: 'bold'}} >
                  No content available for playback
                  </Text>
                )
              }
            </LinearGradient>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ app, download }: GlobalState) => ({ app, download });
export default connect(mapStateToProps)(Download);
