/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CheckCode
// ====================================================

export type CheckCode_reactNative = {
  __typename: "ReactNative",
  id: ?string,
  jwtInfo: ?string,
};

export type CheckCode = {
  reactNative: ?CheckCode_reactNative
};

export type CheckCodeVariables = {
  rnId: string,
  code: string,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================