/* @flow */
import * as React from 'react';
import { store } from '../../redux/store';
import { loader } from 'graphql.macro';
import { ApolloConsumer } from 'react-apollo';
import { connect } from 'react-redux';
import type { GlobalState } from 'store';
import { AUTH_LOGIN } from '../../reducers/auth';

const query = loader('./CheckCode.graphql');

type State = {
  timerObj?: IntervalID,
};

type Props = {
  client: any,
  codeToCheck: string,
} & GlobalState;


class MainView extends React.Component<Props, State> {
  state = {
    timerObj: null,
    pt: null,
  };

  componentDidMount() {
    if (this.state && !this.state.timerObj) {
      let timer = setInterval(this.checkCode, 3000);
      this.setState({
        timerObj: timer      
      });
    }
  }

  componentWillUnmount() {
    if (this.state && !this.state.timerObj) {
      clearInterval(this.state.timerObj);
    }
  }

  checkCode = () => {
    const { client, codeToCheck, app } = this.props;
    client
      .query({
        query,
        fetchPolicy: 'network-only',
        variables: { rnId: app.environment.rnId, code: codeToCheck },
      })
      .then(this.responseHandler);
      // .catch(err => console.log(err));
  }

  responseHandler = ({ data }: {data: CheckCodeQuery}) => { //eslint-disable-line
    const { client } = this.props;
    if (data.reactNative && data.reactNative.jwtInfo) {
      store.dispatch({
        type: AUTH_LOGIN,
        payload: data.reactNative.jwtInfo,
      });
      client.resetStore();
      if (this.state && !this.state.timerObj) {
        clearInterval(this.state.timerObj);
      }
    }
  }

  render() {
    return null;
  }
}

const WrappedMainView = (handledProps: any) => (
  <ApolloConsumer>
    { client => <MainView {...handledProps} client={client} /> }
  </ApolloConsumer>
);
const mapStateToProps = ({ app }) => ({ app });
export default connect(mapStateToProps)(WrappedMainView);
