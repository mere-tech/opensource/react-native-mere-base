import { compose, withState, lifecycle } from 'recompose';
import { withNavigation } from 'react-navigation';
import SearchView from './SearchView';

export default withNavigation(compose(
  withState(false),
  lifecycle({    
    componentDidMount() {
 
    },
    componentWillUnmount() {
    },
  }),
)(SearchView));