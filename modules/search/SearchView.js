import React from 'react';
import { ScrollView, StatusBar, View, Text } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { loader } from 'graphql.macro';
import { connect } from 'react-redux';
import Loading from '../../components/Loading';
import SearchResults from '../../components/SearchResults';
import HeaderLogo from '../../components/HeaderLogo';
import AppBackground from "../../components/AppBackground";

const query = loader('./search.graphql');
const styles = {
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
  scrollContainer: {
    flex: 1
  },
  text: {
    color: "white",
    fontSize: 24,
    marginStart: 20,
  }
};

class HomeViewScreen extends React.Component {
  state = {
    searchText: '',
  };

  updateSearch = searchText => this.setState({ searchText });

  render() {
    const { app } = this.props;
    const { searchText } = this.state;
    return (
      <View style={Object.assign({}, styles.container, app.environment)}>
        <AppBackground />
        <View style={{ height: 35 }} />
        <ScrollView style={styles.scrollContainer} contentContainerStyle={{flex: 1}}>
          <StatusBar hidden />
          <SearchBar
            onChangeText={this.updateSearch}
            placeholder="What should we search for?"
            value={searchText}
          />
          {searchText.length < 3 && (
            <View>
              <HeaderLogo />
          <View style={{ height: 40 }} />
              <Text style={styles.text}>
                Please put your search in above.
              </Text>
            </View>
          )}
          {searchText.length >= 3 && (
            <Loading
              variables={{searchText}}
              query={query}
            >
            {({ data }) => (
              <>
                <View style={{ height: 20 }} />
                { data && (
                  <>
                    <Text style={styles.text}>
                      Showing results for: {searchText}
                    </Text>
                    { data.reactNative
                      && data.reactNative.search
                      && (
                        <SearchResults showSubTitle={false} search={data.reactNative.search} />
                      )
                    }
                  </>
                )}
              </>
            )}
            </Loading>
          )}      

        </ScrollView>
      </View>
    );
  }
};

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(HomeViewScreen);