/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FolderDetails
// ====================================================

export type FolderDetails_reactNative_folder_assets_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type FolderDetails_reactNative_folder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};

export type FolderDetails_reactNative_folder_assets = {
  __typename: "Asset",
  video: ?FolderDetails_reactNative_folder_assets_video,
  subFolder: ?FolderDetails_reactNative_folder_assets_subFolder,
  order: number,
  id: string,
};

export type FolderDetails_reactNative_folder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<FolderDetails_reactNative_folder_assets>,
};

export type FolderDetails_reactNative = {
  __typename: "ReactNative",
  id: ?string,
  folder: FolderDetails_reactNative_folder,
};

export type FolderDetails = {
  reactNative: ?FolderDetails_reactNative
};

export type FolderDetailsVariables = {
  rnId: string,
  folderId?: ?string,
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================