// @flow
import { compose, withState, lifecycle } from 'recompose';

import Orientation from 'react-native-orientation-locker';
import DeviceInfo from 'react-native-device-info';
import MusicControl from 'react-native-music-control';
import { connect } from 'react-redux';
import VideoScreen from './VideoPlayerView';

const VideoView = compose(
  withState('readyToPlay', 'setReadyToPlayState', false),
  lifecycle({
    componentDidMount() {
      MusicControl.enableBackgroundMode(true);
      const { app } = this.props;
      if (app.environment.startVideoLandscape) {
        Orientation.lockToLandscape();
      } else {
        Orientation.unlockAllOrientations();
      }
    },
    componentWillUnmount() {
      const isTablet = DeviceInfo.isTablet();
      if (!isTablet && DeviceInfo.getDeviceType() !== "Tv") {
        Orientation.lockToPortrait();
      } else {
        Orientation.unlockAllOrientations();
      }
    },
  }),
)(VideoScreen);

const mapStateToProps = ({ app }: GlobalState) => ({ app });
const ConnectedVideoView = connect(mapStateToProps)(VideoView);

export default ConnectedVideoView;