import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, Text } from 'react-native';
import { BallIndicator } from 'react-native-indicators';
import Video from 'react-native-video';
import DeviceInfo from 'react-native-device-info';
import MusicControl from 'react-native-music-control';

import { fonts, colors } from '../../styles';

const getPreRoll = (list) => {
  if (list && list.length > 0) {
    const asset = list.find(x => x.metadataKey === "preroll")
    return asset.metadataValue;
  }
  return null;  
}

export default class VideoScreen extends React.Component {
  constructor(props){
    super(props);
    let currentUrl = this.props.navigation.state.params.hls;
    const pre = getPreRoll(this.props.navigation.state.params.mediaAssets);
    let hasPreRoll = false;
    if(pre && !this.props.navigation.state.params.noPreRoll) {
      currentUrl = pre;
      hasPreRoll = true;
    }
    this.state = {
      paused: false,
      hasPreRoll,
      currentUrl,
    }
    const item = this.props.navigation.state.params;
    MusicControl.setNowPlaying({
      title: item.name,
      artwork: item.largeImage, 
      artist: item.name,
      duration: 294, 
      description: '', 
    });
    MusicControl.on('play', this.handlePlay);  
    MusicControl.on('pause', this.handlePause);
  }

  handleEnd = () => {
    const { hasPreRoll } = this.state;
    this.props.navigation.goBack(null);
    if( hasPreRoll ){
      const toPass = {...this.props.navigation.state.params, noPreRoll: true};
      this.props.navigation.navigate({routeName: 'Video',params: toPass})
    }   
  }

  handlePlay = () => {
    this.setState({ paused: false });
    MusicControl.updatePlayback({
      state: MusicControl.STATE_PLAYING,
    })
  }

  handlePause = () => {
    this.setState({ paused: true });
    MusicControl.updatePlayback({
      state: MusicControl.STATE_PAUSED,
    })
  }

  render() {
    return (
      <View style={styles.container}>
        { DeviceInfo.getDeviceType() !== "Tv"
          && !this.props.navigation.state.params.hideBack
          && (
          <TouchableOpacity
            style={{ position: "absolute", top: 80, start: 30, height: 50, width: 50, zIndex: 999}} 
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image resizeMode="center" style={{ width: 30, height: 30 }} source={require('mere-base-app/assets/images/icons/arrow-back.png')} />
          </TouchableOpacity>
        )}
        { !this.props.readyToPlay && (
          <BallIndicator 
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
            }}
            size={80} 
            color={this.props.app.environment.mainColor} 
          />
        )}
        <Video
          resizeMode={"contain"}
          source={{uri: this.state.currentUrl}}
          ref={this.videoPlayer}
          controls={this.props.navigation.state.params.__typename === "Video"}
          poster={this.props.navigation.state.params.__typename === 'Video' ? this.props.navigation.state.params.largeImage : ''}
          audioOnly={this.props.navigation.state.params.__typename === 'Audio'}
          paused={this.state.paused}       
          onEnd={this.handleEnd}
          onProgress={(progress) => {
            this.props.setReadyToPlayState(true);
            MusicControl.updatePlayback({
              state: this.state.paused ? MusicControl.STATE_PAUSED : MusicControl.STATE_PLAYING,
              elapsedTime: progress.currentTime,
              bufferedTime: progress.playableDuration,
            })
          }}
          onReadyForDisplay={() => {
            this.props.setReadyToPlayState(true)
          }}
          ignoreSilentSwitch="ignore"
          playInBackground={DeviceInfo.getDeviceType() !== 'Tv'}
          playWhenInactive={DeviceInfo.getDeviceType() !== 'Tv'}
          allowsExternalPlayback={DeviceInfo.getDeviceType() !== 'Tv'}
          style={styles.backgroundVideo}
        />
        { this.props.navigation.state.params.__typename === "Audio" && (
          <Image
            resizeMethod="auto"
            resizeMode="contain"
            style={{
              height: '100%',
              width: '100%',
              backgroundColor: this.props.app.environment.backgroundColor
            }}
            source={require("mere-base-app/assets/images/phone.png")}
          />
        )}
        { this.props.navigation.state.params.__typename === "Audio" && (
          <TouchableOpacity
            style={{ 
              height: 50,
              width: '100%',
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              marginBottom: 40
            }} 
            onPress={() => { 
              if (this.state.paused) {
                this.handlePlay();
              } else {
                this.handlePause();
              }
            }}
          >
            <Image
              resizeMethod="auto"
              resizeMode="contain"
              style={{ 
                height: '100%',
                width: '100%',
                tintColor: this.props.app.environment.mainColor
              }}
              source={this.state.paused ? require("mere-base-app/assets/images/outline_play.png") : require("mere-base-app/assets/images/outline_pause.png")}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundVideo: {
    flex: 1,
    backgroundColor: "black",
  },
  activityIndicator: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  nerdImage: {
    width: 80,
    height: 80,
  },
  availableText: {
    color: 'white',
    fontSize: 40,
    marginVertical: 3,
  },
  textContainer: {
    alignItems: 'center',
  },
  buttonsContainer: {
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  button: {
    alignSelf: 'stretch',
    marginBottom: 20,
  },
});
