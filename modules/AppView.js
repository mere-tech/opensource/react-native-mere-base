import React from "react";
import { View, ImageBackground } from "react-native";
import { loader } from "graphql.macro";
import { connect } from "react-redux";
import Orientation from "react-native-orientation-locker";
import DeviceInfo from "react-native-device-info";
import RNBootSplash from "react-native-bootsplash";
import { Query } from 'react-apollo';
import { store } from "../redux/store";
import type { GlobalState } from "../redux/reducer";
import { APP_LOADED } from "./AppState";

const query = loader("./App.graphql");

const getVariables = (variables, { environment }) => Object.assign(
  {},
  { rnId:  environment.rnId },
  variables
);

const styles = {
  backgroundImage: {
    backgroundColor: "rgba(52, 0, 0, 0.8)",
    position: "absolute",
    height: "100%",
    width: "100%"
  }
};
class AppView extends React.Component {
  componentDidMount() {
    RNBootSplash.hide({ duration: 250 });
  }

  componentDidUpdate() {
    const { data, app } = this.props;
    if(data && !app.loaded) {
      store.dispatch({
        type: APP_LOADED,
        payload: {
          auth0Audience: data.reactNative.auth0Audience,
          auth0ClientId: data.reactNative.auth0ClientId,
          auth0Domain: data.reactNative.auth0Domain,
          brandingImageUri: data.reactNative.brandingImageUri,
          backgroundImageUri: data.reactNative.backgroundImageUri,
          mainColor: data.reactNative.primaryColor,
          secondColor: data.reactNative.secondaryColor
        }
      });
    } else {
      // store.dispatch({ type: APP_LOADED });
    }
  }

  render() {
    const { app, children } = this.props;

    const isTablet = DeviceInfo.isTablet();
    if (!isTablet && DeviceInfo.getDeviceType() !== "Tv") {
      Orientation.lockToPortrait();
    } else {
      Orientation.unlockAllOrientations();
    }
    if (this.children) {
      return children;
    }
    return null;
  }
}




const mapStateToProps = ({ app }: GlobalState) => ({ app });
const ConnectedAppView = connect(mapStateToProps)(AppView);


const QueryWrap = ({ pollInterval, variables, app, ...rest }) => (
  <Query
    query={query}
    variables={getVariables(variables, app)}
    pollInterval={ pollInterval ? pollInterval : 6000}
  >
    {props => <ConnectedAppView {...props}/> }
  </Query>
);

export default connect(mapStateToProps)(QueryWrap);