import React from 'react';
import { connect } from 'react-redux';
import Auth0 from 'react-native-auth0';
import { View, StyleSheet, Text } from 'react-native';
import { withNavigation } from 'react-navigation';
import AppBackground from "../../components/AppBackground";
import TextButton from '../../components/TextButton';
import { AUTH_LOGOUT, AUTH_LOGIN } from '../../reducers/auth';
import { store } from '../../redux/store';

const Auth0LoginScreen = ({ auth, env, app }: {auth: any, env: any}) => {
  if (app.offlineMode) {
    return (
      <View style={Object.assign({}, styles.container, { backgroundColor: app.environment.backgroundColor })}>
        <AppBackground />
        <Text style={{color: '#FFFFFF', fontSize: 15}}>
          You are not currently connected, to login please connect to the internet.
        </Text>
      </View>
    ); 
  }
  if(env.auth0Domain) {
    const auth0 = new Auth0({
      domain: env.auth0Domain,
      clientId: env.auth0ClientId,
    });
    if (auth.isLoaded && auth.isAuthenticated) {
      return (
        <View style={styles.parent}>
          <AppBackground />
          <View style={styles.container}>
            <Text bold size={18} style={{ color: env.mainColor }}>You are currently Logged In</Text>
            <TextButton
              onPress={() => {
                  store.dispatch({
                    type: AUTH_LOGOUT,
                  });
                }}
              style={{width: 100, marginBottom: 80, top: 20, height: 35}}
            >
              Logout
            </TextButton>
          </View>
        </View>
      )
     }
     return (
       <View style={styles.parent}>
         <AppBackground />
         <View style={styles.container}>
           <Text bold size={18} style={{ color: env.mainColor }}>You are currently Logged Out</Text>
           <TextButton
             onPress={() => {
              auth0.webAuth
              .authorize({scope: 'openid email profile'})
              .then(credentials => { 
                store.dispatch({
                  type: AUTH_LOGIN,
                  payload: credentials.idToken,
                });
              })
            }}
             style={{width: 100, marginBottom: 80, top: 20, height: 35}}
           >
            Login
           </TextButton>
         </View>
       </View>
      );
  }

  return (
    <View style={Object.assign({}, styles.parent, env)}>
      <AppBackground />
      <View style={styles.container}>
        <Text bold size={18} style={{ color: env.mainColor }}>You are currently in offline mode.</Text>        
      </View>
    </View>
   );
}

const styles = StyleSheet.create({
  parent: {
    flex: 1,
    flexDirection: 'row',
  },
  container: {
    height: "100%",
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center',
  }
})

const withNav = withNavigation(Auth0LoginScreen);
const mapStateToProps = ({ app, auth }) => ({ auth, app, env: app.environment });
const withConnect = connect(mapStateToProps)(withNav);

export default withConnect;