// @flow
export type AppStateType = {
  isFirstOpen: boolean,
  brandingImageUri: ?string,
  navBarIconTintColor: ?string,
  selectedItem: ?string,
  offlineMode: boolean,
  loaded: boolean,
  environment: {
    auth0Audience: string,
    auth0ClientId: string,
    auth0Domain: string,
    graphqlEndpoint: string,
    rnId: string,
    playIcon: any,
    mainColor: string,
    secondColor: string,
    thirdColor: string,
    backgroundColor: string,
    startVideoLandscape: boolean,
    showDescriptionInRow: boolean,
    showFeatured: boolean,
    overrideFeaturedText: ?String,
    listViewForFolder: boolean,
  }
};

type ActionType = {
  type: string,
  payload?: any
};

export const initialState: AppStateType = {
  isFirstOpen: true,
  brandingImageUri: "",
  navBarIconTintColor: "#DD5C30",
  selectedItem: null,
  offlineMode: true,
  backgroundImageUri: "",
  loaded: false,
  environment: {
    auth0Audience: "",
    auth0ClientId: "",
    auth0Domain: "",
    graphqlEndpoint: "",
    rnId: "NOTSET",
    playIcon: "",
    mainColor: "#FFFFFF",
    secondColor: "#FFFFFF",
    thirdColor: "#FFFFFF",
    backgroundColor: '#000000',
    startVideoLandscape: true,
    showDescriptionInRow: true,
    listViewForFolder: false,
  }
};

export const SET_FIRST_OPEN = "AppState/SET_FIRST_OPEN";
export const APP_LOADED = "AppState/APP_LOADED";
export const SET_SELECTED_ITEM = "AppState/SET_SELECTED_ITEM";
export const SET_ENVIRONMENT = "AppState/SET_ENVIRONMENT";
export const SET_OFFLINE_OR_ONLINE = "AppState/SET_OFFLINE_OR_ONLINE";

export function setAppOpened(): ActionType {
  return {
    type: SET_FIRST_OPEN
  };
}

export default function AppStateReducer(
  state: AppStateType = initialState,
  action: ActionType
): AppStateType {
  switch (action.type) {
    case SET_OFFLINE_OR_ONLINE:
      return {
        ...state,
        offlineMode: action.payload,
      };
    case SET_ENVIRONMENT:
      return {
        ...state,
        environment: Object.assign({}, state.environment, action.payload)
      };
    case SET_FIRST_OPEN:
      return {
        ...state,
        isFirstOpen: false
      };
    case APP_LOADED:
      if (action.payload) {
        return {
          ...state,
          brandingImageUri: action.payload.brandingImageUri,
          backgroundImageUri: action.payload.backgroundImageUri,
          environment: Object.assign({}, state.environment, {
            mainColor: action.payload.mainColor,
            secondColor: action.payload.secondColor,
            auth0Domain: action.payload.auth0Domain,
            auth0ClientId: action.payload.auth0ClientId,
            auth0Audience: action.payload.auth0Audience,
          }),
          loaded: true,
        };
      }
      return state;
    case SET_SELECTED_ITEM:
      if (action.payload) {
        return {
          ...state,
          selectedItem: action.payload.selectedItem
        };
      }
      return state;
    default:
      return state;
  }
}
