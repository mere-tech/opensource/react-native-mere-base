/* @flow */
import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import FolderList from './FolderList';
import { connect } from 'react-redux';
import { colors, fonts } from '../styles';
import type { GridFolderDetails } from '../lib/graphql/__generated__/GridFolderDetails';

type InputProp = {
  folder: GridFolderDetails,
  navigation: any,
};

const Grid = ({ app, folder, ...rest }: InputProp) => (
  <ScrollView removeClippedSubviews>
    { folder.assets
      && folder.assets.map( fass => {
        if (fass && fass.subFolder) {
          return (
            <View>
              <Text
                style={{
                  fontFamily: fonts.primaryRegular,
                  fontSize: 26,
                  paddingLeft: 30,
                  color: app.environment.secondColor
                }}
              >
                {fass.subFolder.name}
              </Text>
              <FolderList
                {...rest}
                folder={fass.subFolder}
                key={fass.id}
              />
            </View>
          );
        }
        return null;          
      })
    }
  </ScrollView>
)

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(Grid)