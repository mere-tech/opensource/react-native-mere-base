/* @flow */
import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import VerticalFolderList from "./VerticalFolderList";
import FolderItem from './FolderItem';
import VideoItem from './VideoItem';
import FolderList from './FolderList';

type InputProp = {
  folder: any,
  navigation: any,
};

// create a helper function to make it look like a normal folder with a unique id
const uuidv4 = () => 
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });

const convertSearch = (search) => ({
    id: uuidv4(),
    name: "",
    assets: search.map(({ video, folder }) => ({
      video,
      subFolder: folder,
    })),
  });

export default ({ search, ...rest }: InputProp) => {
  if (search) {
    const folder = convertSearch(search);
    return (
      <ScrollView removeClippedSubviews>
        <View style={{ top: 30, paddingHorizontal: 30, paddingBottom: 90, flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
        { folder.assets
          && folder.assets.map( fass => {
            if (fass && fass.video) {
              return <VideoItem itemStyle={styles.item} key={fass.id} item={fass.video} />;
            }
            if (fass && fass.subFolder) {
              return <FolderItem key={fass.id} item={fass.subFolder} />;
            }
            return null;          
          })
        }
      </View>
      </ScrollView>
    );
  }
  return null;
};
const styles = StyleSheet.create({
  item: {

  },
  container: {
    top: 0,
    bottom: 0,
    height: '100%',
    width: '100%',

    backgroundColor: 'transparent',
  },
})
