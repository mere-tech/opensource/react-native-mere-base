/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { ScrollView, View, FlatList, Text } from 'react-native';
import HorizontalScrollView from './HorizontalScrollView';
import VideoItem from './VideoItem';
import FolderItem from './FolderItem';

import type { FolderExtendedDetails } from '../lib/graphql/__generated__/FolderExtendedDetails';
import type { GridFolderDetails_assets_subFolder } from '../lib/graphql/__generated__/GridFolderDetails';

type InputProp = {
  folder: FolderExtendedDetails | GridFolderDetails_assets_subFolder,
};

export default ({ folder, ...rest }: InputProp) => (
  <ScrollView
    overScrollMode='never'
  >
    <FlatList
      horizontal
      removeClippedSubviews={true}
      style={{
        start: 0,
        flex: 1,
      }}
      data={folder.assets}
      renderItem={({item}) => {
        if (item && item.video) {
          return <VideoItem key={item.id} item={item.video} />;
        }
        if (item && item.subFolder) {
          return <FolderItem key={item.id} item={item.subFolder} />;
        }
        return null;          
      }}
      keyExtractor={item => item.id}
    />    
  </ScrollView>
)