/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, TouchableHighlight, Image, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import { Text } from './StyledText';
import { colors } from '../styles';

type InputProp = {
  text: string,
  env: any,
  underlayColor: string,
  onPress: () => any,
};

class TextButton extends React.Component<InputProp> {
  state = {
    showBorder: false
  }

  render() {
    return (
      <TouchableHighlight
            onHideUnderlay={() => {
              this.setState({ showBorder: false })
            }}
            onShowUnderlay={() => {
              this.setState({ showBorder: true })
            }}
            activeOpacity={0.9}
            underlayColor={this.props.env.thirdColor}
            onPress={this.props.onPress}
            style={this.props.style}
          >
            <View 
              style={{ 
                backgroundColor: this.state.showBorder ? this.props.env.mainColor : colors.transparent, 
                borderWidth: 2, 
                borderRadius: 1, 
                borderColor: this.state.showBorder ? colors.transparent : this.props.env.mainColor,
                height: 35
              }} 
            >
              <Text
                style={{
                  color: this.state.showBorder ? colors.transparent: this.props.env.mainColor,
                  alignSelf: 'center',
                  padding: 5,
                }}>
                {this.props.children}
              </Text> 
            </View>
          </TouchableHighlight>
    )
  }
}

const mapStateToProps = ({ app }) => ({ env: app.environment });
const withConnect = connect(mapStateToProps)(TextButton)
export default withNavigation(withConnect);