/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';
import { Text } from './StyledText';

type Prop = {
 title?: ?string,
 subTitle?: ?string,
 showTitle?: boolean,
 showSubTitle?: boolean,
 children: any,
 env: any,
 viewStyle: {
   height?: string,
   width?: string,
   top?: string,
   left?: string,
 } | any,
}

const baseStyle = {

};
const merge = (a,b) => Object.assign({},a,b);

const HorizontalScrollView = ({
  title,
  subTitle,
  children,
  viewStyle,
  showTitle,
  showSubTitle,
  env,
}: Prop) => (
  <View style={merge(baseStyle, viewStyle)}>
    <View>
      { showTitle && (<Text bold size={22} style={{ color: env.mainColor, start: 30, paddingBottom: 15 }}>{title.toUpperCase()}</Text>)}
      { showSubTitle && (<Text size={18} style={{ color: env.secondColor }}>{subTitle}</Text>)}
    </View>
    <ScrollView horizontal>
      {children}
    </ScrollView>
  </View>
);
const mapStateToProps = ({ app }) => ({ env: app.environment });
const withConnect = connect(mapStateToProps)(HorizontalScrollView)

export default withConnect;
