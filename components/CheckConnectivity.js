import React, { Component } from "react";
import { store } from '../redux/store';
import { SET_OFFLINE_OR_ONLINE } from '../modules/AppState';
import NetInfo from "@react-native-community/netinfo";

export default class CheckConnectivity extends Component {
    state = {
        listener: null,
    };

    componentDidMount() {
        const listener = NetInfo.addEventListener(state => {
            store.dispatch({
                type: SET_OFFLINE_OR_ONLINE,
                payload: !state.isConnected,
              });
        });
        this.setState({listener});
    }

    componentWillUnmount() {
        const { listener } = this.state;
        if(listener) {
            listener();
            this.setState({listener: null});
        }
    }

  render() {
    return null;
  }
}