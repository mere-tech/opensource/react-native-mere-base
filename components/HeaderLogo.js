/* @flow */
import React from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import type { GlobalState } from '../redux/reducer';

const HeaderLogo = ({ app }: GlobalState) => (
  <Image
    style={{
      marginStart: 30,
      marginTop: 60,
      height: 85,
      width: 500 
    }}
    source={{ uri: app.brandingImageUri }}
    resizeMode="contain"
  />
);

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(HeaderLogo);