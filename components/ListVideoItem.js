/**
 * @flow
 */

import React from "react";
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import { withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';
import { downloadVideo, deleteLocalItem } from '../lib/Video';
import type { CompositeVideoItem } from '../lib/Video';
import { colors, fonts } from '../styles';

type Props = {
  video: CompositeVideoItem,
  showPoster: ?boolean,
  showDelete: ?boolean,
  app: any,
  navigation: any,
};

const GetPercent = (video, download) => {
  const foundVideo = download.videosMap[video.id];
  // console.log(foundVideo);
  if (foundVideo && foundVideo.percentDownloaded < 0.95) {
    return Math.ceil(foundVideo.percentDownloaded * 100);
  }
  if (foundVideo && foundVideo.percentDownloaded >= 0.9) {
    return 100;
  }
  if (foundVideo) {
    return 0;
  }
  return -1;
}

const ListVideoItem = ({showDelete, download, video, app, showPoster, navigation, showDownload}: Props) => (
  <View
    style={{
      // flex: 1,
      // alignItems: 'center',
      fontFamily: fonts.primaryRegular,
      flexDirection: 'row',
      marginTop: 0,
      justifyContent: 'flex-start',
    }}
  >
    { showPoster && (
   
      <TouchableOpacity
            style={{
              marginTop: 20,
              flex: 2,
              flexDirection: 'row',
            }}
            onPress={() => { navigation.navigate({routeName: 'Video', params: { ...video }})}}
          >
            <View
              style={{
                flex: 2,
                flexDirection: 'row',
                // alignItems: 'center',
                marginRight: 10,
                marginLeft: 10,
                alignItems: "center"
              }}
            >  
              <Image
                style={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                }}
                resizeMode='contain'
                resizeMethod='resize'
                source={{uri: video.largeImage}}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Icon
                  name='play'
                  color={app.environment.mainColor}
                  type='font-awesome'
                />
              </View>                    
          </View>
          <View
            style={{
              flex: 3,
              // alignItems: 'center',
              alignItems: "flex-start"
            }}
          >
            <Text
              numberOfLines={1}
              style={{
                flex: 1,
                fontFamily: fonts.primaryRegular,
                color: app.environment.mainColor,
                marginRight: 10,
                fontSize: 18,
              }}
            >
              {video.name}
            </Text>
            <Text
              numberOfLines={2}
              style={{
                flex: 1,
                fontFamily: fonts.primaryRegular,
                color: app.environment.secondColor,
                marginRight: 10,
                fontSize: 18,
              }}
            >
              {video.description}
            </Text>          
          </View> 

      </TouchableOpacity>
    )}


    { !showPoster && (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          // alignItems: 'center',
          marginTop: 10,
          justifyContent: 'flex-start',
        }}
      >
        <Text
          numberOfLines={1}
          style={{
            flex: 5,
            marginTop: 10,
            marginLeft: showPoster ? 0 : 10,
            color: app.environment.mainColor,
            // textAlignVertical: 'center'
          }}
        >
          {video.name}
        </Text>
        <View
          style={{
            flex: 2,
            flexDirection: 'row',
            justifyContent: 'center',
            // alignItems: 'center',
            marginTop: 10,
            marginRight: 30,
            justifyContent: 'flex-end',
          }}
        >
          { GetPercent(video, download) !== 100
            && GetPercent(video, download) !== -1
            && (
            <Text
              style={{
                color: app.environment.mainColor,
                marginRight: 15,
                fontSize: 18,
                // textAlignVertical: 'center'
              }}
            >
              {GetPercent(video, download)}%
            </Text>
          )}
          { showDelete && (
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={() => { deleteLocalItem(video, download)}}
            >
              <Icon
                style={{
                  flex: 1,
                }}
                name='trash'
                color={app.environment.mainColor}
                type='font-awesome'
              />
            </TouchableOpacity>
          )}      
          { showDownload
            && GetPercent(video, download) === -1  && (
            <TouchableOpacity
                style={{
                  marginRight: 20,
                }}
                onPress={() => {
                  downloadVideo(video);
                }}
              >
                <Icon
                  name='cloud-download'
                  color={app.environment.mainColor}
                  type='font-awesome'
                />
            </TouchableOpacity>
          )}
          { (GetPercent(video, download) === 100 || !showDelete)
            && (
            <TouchableOpacity
              onPress={() => { navigation.navigate({routeName: 'Video', params: { ...video }})}}
            >
              <Icon
                style={{
                  flex: 3,
                }}
                name='play'
                color={app.environment.mainColor}
                type='font-awesome'
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    )}        
  </View>
)

const mapStateToProps = ({ app, download }: GlobalState) => ({ app, download });
export default connect(mapStateToProps)(withNavigation(ListVideoItem));